Debian package building scripts.

These scripts live in [swh-jenkins-jobs][] in the scripts/debian subdirectory
and are updated by a [jenkins job][].

[swh-jenkins-jobs]: https://gitlab.softwareheritage.org/swh/infra/ci-cd/swh-jenkins-jobs
[jenkins job]: https://jenkins.softwareheritage.org/debian/maintenance/update-scripts
